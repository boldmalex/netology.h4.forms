import react, { useState } from "react";
import TrainingAddEdit from "../training-add-edit/training-add-edit";
import TrainingList from "../training-list/training-list";
import TrainingModel from "../training-model/training-model";
import { nanoid } from "nanoid";
import propTypes from "prop-types";


const Trainings = ({items}) => {

    const [currentItems, setCurrentItems] = useState(items);
    const [editing, setEditing] = useState({
        isInserting: true,
        item: new TrainingModel(nanoid(), null, null)
    });


    const handleAddItem = (item) => {
        setCurrentItems(prevItems => [...prevItems, item]);
        handleEndEdit();
    }


    const handleEditItem = (item) => {
        setCurrentItems(prevItems => 
            prevItems.map(prevItem => (prevItem.id === item.id ? item : prevItem))
        );
        handleEndEdit();
    }


    const handleRemoveItem = (id) => {
        setCurrentItems(prevItems => 
            prevItems.filter((item) => item.id !== id)
        );
    };


    const handleBeginEdit = (item) => {
        setEditing({isInserting: false, item: item});
    };


    const handleEndEdit = () => {
        setEditing({isInserting: true, item: new TrainingModel(nanoid(), null, null)});
    }

    
    return(
        <div>
            <TrainingAddEdit handleAddSubmit={handleAddItem} 
                             handleEditSubmit={handleEditItem} 
                             item={editing.item} 
                             isInserting={editing.isInserting}/>

            <TrainingList items={currentItems} handleRemove={handleRemoveItem} handleEdit={handleBeginEdit}/>
        </div>
    );

};


propTypes.Trainings = {
    item: propTypes.instanceOf(TrainingModel)
}

export default Trainings;