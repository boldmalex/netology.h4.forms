const stringToDate = (strDate) =>{
    var parts = strDate.split(".");
    var date = new Date(parseInt(parts[2], 10),
                        parseInt(parts[1], 10) - 1,
                        parseInt(parts[0], 10));

    return date;
};

export default stringToDate;