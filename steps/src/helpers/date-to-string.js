const dateToString = (date) =>{
    if (date != null)
        return date.toLocaleDateString("ru-Ru", {
            year:"2-digit",
            month:"2-digit",
            day:"2-digit",
        });
    else
        return "";
};


export default dateToString;
