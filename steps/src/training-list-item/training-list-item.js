import react from "react";
import "./training-list-item.css";
import dateToString from "../helpers/date-to-string.js";
import propTypes from "prop-types";
import TrainingModel from "../training-model/training-model";


const TrainingListItem = ({item, handleRemove, handleEdit}) => {

    const onRemoveClick = () =>{
        handleRemove(item.id);
    };

    const onEditClick = () =>{
        handleEdit(item);
    }


    return (
        <div className="training-item-container">
            <div className="training-item-field">
            <span>{dateToString(item.date)}</span>
            <span>{item.len}</span>
            </div>
            <div className="training-item-field training-item-field-buttons">
                <button
                    type="button"
                    className="btn btn-outline-success btn-sm btn-training"
                    onClick={onEditClick}>
                    <i className="bi bi-pen"/>
                </button>
                <button
                    type="button"
                    className="btn btn-outline-danger btn-sm btn-training"
                    onClick={onRemoveClick}>
                    <i className="bi bi-x"></i>
                </button>
            </div>
        </div>
    );
}


propTypes.TrainingListItem = {
    item: propTypes.instanceOf(TrainingModel),
    handleRemove: propTypes.func,
    handleEdit: propTypes.func
}

export default TrainingListItem;