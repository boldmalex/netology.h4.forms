import react from "react";
import Trainings from "./trainings/trainings.js";
import data from "./data.js";


function App() {
  return (
      <>
        <Trainings items={data}/>
      </>
  );
}

export default App;