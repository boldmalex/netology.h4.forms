import react from "react";
import TrainingListItem from "../training-list-item/training-list-item.js";
import "./training-list.css";
import propTypes from "prop-types";
import TrainingModel from "../training-model/training-model.js";


const TrainingList = ({items, handleRemove, handleEdit}) => {

    const elements = items.map((item) =>{
        return(
            <li key={item.id}>
                <TrainingListItem item = {item} handleRemove = {handleRemove} handleEdit = {handleEdit}/>
            </li>
        );
    });


    if (items.length > 0)
    {
        return (
            <div className="training-list-container">
                <ul>
                    {elements}
                </ul>
            </div>
        );
    }
    else
        return null;
};


propTypes.TrainingList = {
    item: propTypes.arrayOf(TrainingModel),
    handleRemove: propTypes.func,
    handleEdit: propTypes.func,
}

export default TrainingList;