import { nanoid } from "nanoid";
import TrainingModel from "./training-model/training-model";

const data = [ new TrainingModel(nanoid(), new Date(2021, 11, 15), 21.1),
               new TrainingModel(nanoid(), new Date(2021, 10, 9), 13.5)
             ];

export default data;