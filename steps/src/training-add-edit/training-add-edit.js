import react, { useEffect, useState } from "react";
import "./training-add-edit.css";
import dateToString from "../helpers/date-to-string.js";
import stringToDate from "../helpers/string-to-date.js";
import TrainingModel from "../training-model/training-model";
import propTypes from "prop-types";


const TrainingAddEdit = ({item, handleAddSubmit, handleEditSubmit, isInserting}) => {

    const [currentItem, setCurrentItem] = useState({ 
        id: item.id, 
        date: dateToString(item.date), 
        len: item.len == null ? "" : item.len.toString(),
    });

    useEffect(() =>{
        setCurrentItem({
            id: item.id, 
            date: dateToString(item.date), 
            len: item.len == null ? "" : item.len.toString(),
        });
    }, [item]);

    
    const onChangeItem = ({target}) =>{
        const name = target.name;
        const value = target.value;
        setCurrentItem(prevItem => ({...prevItem, [name]: value}));
    };


    const onAdding = (evt) => {
        evt.preventDefault();
        var addingItem = new TrainingModel(currentItem.id, 
                                           stringToDate(currentItem.date), 
                                           parseFloat(currentItem.len));
        handleAddSubmit(addingItem);
    };


    const onEditing = (evt) => {
        evt.preventDefault();
        var editingItem = new TrainingModel(currentItem.id, 
                                            stringToDate(currentItem.date), 
                                            parseFloat(currentItem.len));
        handleEditSubmit(editingItem);
    };


    return (
        <form onSubmit={isInserting ? onAdding : onEditing}>
            <div className="container">
                <div className="training-editor-field" >
                        <div><label htmlFor="date">(ДД.ММ.ГГ): </label></div>
                        <div><input  type="text"
                                id="date"
                                name="date"
                                value={currentItem.date}
                                onChange={onChangeItem}/>
                        </div>
                </div>

                <div className="training-editor-field">
                        <div><label htmlFor="len">Пройдено км: </label></div>
                        <div><input  id="len"
                                name="len"
                                value={currentItem.len}
                                onChange={onChangeItem}/>
                        </div>
                </div>

                <div className="training-editor-field">
                    <br/>
                    <button className="training-editor-field-button" type="submit">Ok</button>
                </div>
            </div>
        </form>
    );
};

propTypes.TrainingAddEdit = {
    item: propTypes.instanceOf(TrainingModel),
    handleAddSubmit: propTypes.func,
    handleEditSubmit: propTypes.func,
    isInserting: propTypes.bool
}

export default TrainingAddEdit;