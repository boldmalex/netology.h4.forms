import react, { useState } from "react";
import "./hex2rgb.css";


const Hex2rgb = () =>{
    
    const [color, setColor] = useState({
        colorHex:"",
        colorRgb:"",
        isDone: null
    });


    const regexHexColor = new RegExp("^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$");


    const hexTorgb = (hex) =>{
        const temp = regexHexColor.exec(hex);
        return temp ? 
                `rgb(${parseInt(temp[1], 16)}, ${parseInt(temp[2], 16)}, ${parseInt(temp[3], 16)})`
                : null;
    };


    const getBgColorStyle = () => {
        if (color.isDone != null)
            return {'backgroundColor': color.isDone ? color.colorHex : "#E94B35"};
        else
            return {'backgroundColor': "#33495F"};
    };
    

    const handleColorHexChange = (evt) => {

        const currentValue = evt.target.value;

        if (currentValue.length === 7)
        {
            if (regexHexColor.test(currentValue))
                setColor(prevColor => ({
                    ...prevColor, colorHex: currentValue, colorRgb: hexTorgb(currentValue), isDone: true
                }));
            else
                setColor(prevColor => ({
                    ...prevColor, colorHex: currentValue, colorRgb: "Ошибка!", isDone: false
                }));
        }
        else
            setColor(prevColor => ({
                ...prevColor, colorHex: currentValue, colorRgb:"", isDone: null
            }));
    };


    return (
        <div className= "container" style={getBgColorStyle()}>
            <div className="container-item">
                <div>
                    <input  id= "colorHex" 
                            name= "colorHex" 
                            type= "text"
                            value= {color.colorHex}
                            onChange= {handleColorHexChange}/>
                </div>
                <div>
                    <input  id= "colorRgb" 
                            name= "colorRgb" 
                            readOnly={true}
                            type= "text"
                            value= {color.colorRgb}/>
                    
                </div>
            </div>
        </div>
    );
};

export default Hex2rgb;